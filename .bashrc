if [ -f ~/.standard_bashrc ]; then
	. ~/.standard_bashrc
else
	echo "[Warning!] Can't load '~/.standard_bashrc'"
fi

umask u=rwx,g=rx,o=rx

prompt()
{
	local prompt_block1="\e[48;5;25m\e[38;5;16m \u@\h "
	local prompt_block2="\e[48;5;26m\e[38;5;16m {\j} "
	local prompt_block3="\e[48;5;27m\e[38;5;16m `pwd` "
	local prompt_block4="\e[48;5;63m\e[38;5;16m (`ls | wc -l`) "

	PS1="\e[1m${prompt_block1}${prompt_block2}${prompt_block3}${prompt_block4}\e[0m\n\$ "
}
PROMPT_COMMAND=prompt

power()
{
	for b in /sys/class/power_supply/BAT?/capacity;
	do
		BAT+="$BAT$(cat $b)% "
	done && echo "battery power: $BAT"
	BAT=
}

alias cls="tput reset"
alias wb="tmux new-session -A -s 'wb'"
alias nnn="nnn -c -o"

bind '"\C-r":"tput reset\n"'
bind '"\C-e":"exit\n"'

stty -ixon

