" Dirk Schwartzer's .vimrc v1.0.32b

set noswapfile
set encoding=utf-8
set fileencoding=utf-8
set noexpandtab
set tabstop=4
set softtabstop=4
set shiftwidth=4

set nocompatible
set wildmenu
set wildmode=full
set mouse=a
set title
set laststatus=2
set whichwrap=b,s,<,>,[,]
set showcmd
set showmode
set number
set incsearch
set splitbelow
set splitright
set nowrap

let g:netrw_banner = 0
let g:netrw_liststyle = 1
let g:netrw_browse_split = 0
let g:netrw_winsize = 20

syntax on

function s:HlModification()
	hi StatusLine term=bold cterm=bold,reverse ctermfg=DarkGrey ctermbg=White
	hi StatusLineNC term=reverse cterm=reverse ctermfg=DarkGrey ctermbg=Black
	hi VertSplit term=bold cterm=bold ctermfg=DarkGrey
	hi ModeMsg term=bold cterm=bold ctermfg=White ctermbg=DarkRed
	hi TabLine term=bold cterm=bold ctermfg=White ctermbg=DarkGrey
	hi TabLineSel term=bold cterm=bold ctermfg=Black ctermbg=White
	hi TabLineFill term=bold cterm=bold ctermbg=DarkGrey
endfunction

function SetColorScheme(scheme)
	execute "colorscheme ".a:scheme
	call s:HlModification()
endfunction

function ShowStatusLine()
	set statusline=%(\ %m\%r%)\ %t\ (%<%{expand('%:p:h')})%=\ \[\ %{&ff}\ \]%(\ %Y\ \|%)\ \ %l\/\%c\ \ %P\ 
endfunction

function SetNormalTextColor(color)
	let g:normal_text_color = a:color
	execute "hi Normal ctermfg=".g:normal_text_color
	execute "autocmd ColorScheme * hi Normal ctermfg=".g:normal_text_color
endfunction

function RunCustomScript()
	let l:vimconf = expand('~/.vimconf')
	if filereadable(l:vimconf)
		execute 'source '.l:vimconf
	else
		execute 'set background=dark'
		execute 'colorscheme elflord'
	endif
	call s:HlModification()
	call ShowStatusLine()
endfunction

function SaveAs()
	let l:file_name = input("Save as: ", getreg("%"))
	if 0 == strlen(l:file_name)
		echo "canceled!"
		return
	endif
	execute ":w ".l:file_name
endfunction

function GotoLine()
	let l:line_num = input("Go to: ")
	if 0 == strlen(l:line_num)
		echo "canceled!"
		return
	endif
	execute ":".l:line_num
endfunction

function Find()
	let l:search_term = input("Find text (\"s):", getreg("s"))
	if 0 == strlen(l:search_term)
		echo "canceled!"
		return
	endif
	let @/ = escape(l:search_term, "/\\")
	let @s = escape(l:search_term, "/\\")
	execute "normal! /".escape(l:search_term, "/\\")
	execute "normal! n"
	echo ""
endfunction

function Replace()
	let l:search_term = input("Replace text :", getreg("s"))
	if 0 == strlen(l:search_term)
		echo "canceled!"
		return
	endif
	let l:search_term = escape(l:search_term, "/\\")	

	let l:replace_term = input("with text :", getreg("r"))
	let l:replace_term = escape(l:replace_term, "/\\")	

	echo "\n"
	let l:replace_option = inputlist(
							\ [
							\ 'select replace option: ',
							\ '(0) <cancel!>',
							\ '(1) case sensitive',
							\ '(2) case insensitiv',
							\ '(3) whole word - case sensitive',
							\ '(4) whole word - case insensitive'
							\ ])
	if 0 == l:replace_option
		echo "\ncanceled!"
		return
	elseif 1 == l:replace_option
		let l:replace_flags = input("(case sensitive) enter flags :", "gc")
	elseif 2 == l:replace_option
		let l:replace_flags = input("(case insensitiv) enter flags :", "gci")
	elseif 3 == l:replace_option
		let l:replace_flags = input("(whole word - case sensitive) enter flags :", "gc")
		let l:search_term = "\\<".l:search_term."\\>"
	else
		let l:replace_flags = input("(whole word - case insensitive) enter flags :", "gci")
		let l:search_term = "\\<".l:search_term."\\>"
	endif
	redraw!

	let l:expression = ".,$s/".l:search_term."/".l:replace_term."/".l:replace_flags 
	echo ":".l:expression
	execute l:expression
	let @/ = l:search_term
	let @s = l:search_term
	let @r = l:replace_term
endfunction

function EditRegister()
	registers
	let l:register = input("register name :", "")
	if 0 == strlen(l:register)
		echo "canceled!"
		return
	endif
	let l:clip = input("write to register \"".l:register.":", getreg(l:register))
	execute "let @".l:register." = '".escape(l:clip, "'")."'"
endfunction

function CopyRegister()
	registers
	let l:src = input("source register :", "")
	if 0 == strlen(l:src)
		echo "canceled!"
		return
	endif
	let l:dest = input("destination register :", "")
	if 0 == strlen(l:dest)
		echo "canceled!"
		return
	endif
	execute "let @".l:dest." = @".l:src
endfunction

function StoreClipboard()
	registers
	let l:dest = input("store \"c to :", "")
	if 0 == strlen(l:dest)
		echo "canceled!"
		return
	endif
	execute "let @".l:dest." = @c"
endfunction

function RestoreClipboard()
	registers
	let l:src = input("restore \"c with :", "")
	if 0 == strlen(l:src)
		echo "canceled!"
		return
	endif
	execute "let @c = @".l:src
endfunction

function ManageBuffer(desc, cmd)
	echo "-- Buffers! --"
	execute "buffers"
	let l:bufnr = input(a:desc)
	if 0 == l:bufnr
		echo "canceled!"
		return
	endif
	execute a:cmd." ".l:bufnr
endfunction

function ChWDir()
	let l:dir = expand("%:p:h")
	execute "cd ".l:dir
endfunction

function LocalChWDir()
	let l:dir = expand("%:p:h")
	execute "lcd ".l:dir
endfunction

function GetChar()
	let l:key = getchar()
	while 0 == l:key
		let l:key = getchar()
	endwhile
	return l:key
endfunction

function CommandMenu(prompt, menu)
	echo a:prompt
	let l:cmd = 0
	while 27 != l:cmd && 13 != l:cmd
		let l:cmd = GetChar()
		for l:i in a:menu
			if l:i[0] == l:cmd
				redraw!
				execute l:i[1]
				return 1
			endif
		endfor
	endwhile
	redraw!
	return 0
endfunction

function FileFormatCommands()
	call CommandMenu("-- FileFormat! -- /u:unix /d:dos /1:dos2unix /2:unix2dos",
		\ [
		\ [117, 'setlocal fileformat=unix'],
		\ [100, 'setlocal fileformat=dos'],
		\ [49, 'e ++fileformat=dos | setlocal fileformat=unix | w'],
		\ [50, 'e ++fileformat=dos | w']
		\ ])
endfunction

function FileBufferCommands()
	echo "-- Buffers! -- /s:show /n:show_new_hwin /N:show_new_vwin /t:show_new_tab /c:close"
	execute "buffers"
	call CommandMenu("",
		\ [
		\ [115, 'call ManageBuffer("show buffer with number: ", "confirm buffer")'],
		\ [110, 'call ManageBuffer("new horizontal window and show buffer with number: ", "new | buffer")'],
		\ [78, 'call ManageBuffer("new vertical window and show buffer with number: ", "vnew | buffer")'],
		\ [116, 'call ManageBuffer("new tab and show buffer with number: ", "tabnew | buffer")'],
		\ [99, 'call ManageBuffer("close buffer with number: ", "confirm bwipeout")']
		\ ])
endfunction

function FileCommands()
	call CommandMenu("-- File! -- /r:recent /o:open /n:new_open /N:vnew_open /t:new_tab_open | /b:buffers /w:write /a:save_all /s:save_as | /c:chwdir /l:lchwdir | /f:file_format",
		\ [
		\ [114, 'browse oldfiles'],
		\ [111, 'Explore'],
		\ [110, 'new | Explore'],
		\ [78, 'vnew | Explore'],
		\ [116, 'tabnew | Explore'],
		\ [98, 'call FileBufferCommands()'],
		\ [119, 'w'],
		\ [97, 'wall'],
		\ [115, 'call SaveAs()'],
		\ [99, 'call ChWDir()'],
		\ [108, 'call LocalChWDir()'],
		\ [102, 'call FileFormatCommands()']
		\ ])
endfunction

function ClipboardCommands()
	call CommandMenu("-- Clipboard! -- /c:copy /e:edit | /s:store /r:restore | /o:open /v:vopen",
		\ [
		\ [99, 'call CopyRegister()'],
		\ [101, 'call EditRegister()'],
		\ [115, 'call StoreClipboard()'],
		\ [114, 'call RestoreClipboard()'],
		\ [111, 'new ~/.vimclip'],
		\ [118, 'vnew ~/.vimclip']
		\ ])
endfunction

function EditCommands()
	call CommandMenu("-- Edit! -- /a:select_all /g:goto /f:find /r:replace /e:re-edit",
		\ [
		\ [97, 'normal! ggVG'],
		\ [103, 'call GotoLine()'],
		\ [102, 'call Find()'],
		\ [114, 'call Replace()'],
		\ [101, 'edit!']
		\ ])
endfunction

function WindowCommands()
	call CommandMenu("-- Windows! -- /n:new /N:vnew | /s:split /v:vsplit | /o:close_others",
		\ [
		\ [110, 'new'],
		\ [78, 'vnew'],
		\ [115, 'split'],
		\ [118, 'vsplit'],
		\ [111, 'only']
		\ ])
endfunction

function TabCommands()
	call CommandMenu("-- Tabs! -- /n:new /c:close /o:close_others",
		\ [
		\ [110, 'tabnew | call LocalChWDir()'],
		\ [99, 'tabclose'],
		\ [111, 'tabonly']
		\ ])
endfunction

function NormalTextColorMenu()
	let l:retv = 1
	while 1 == l:retv
		let l:retv = CommandMenu("-- NormalTextColor! -- /1:grey /2:white /3:blue /4:yellow /5:black /6:cyan /7:green /8:dark_green",
		\ [
		\ [49, 'call SetNormalTextColor("Grey")'],
		\ [50, 'call SetNormalTextColor("White")'],
		\ [51, 'call SetNormalTextColor("Blue")'],
		\ [52, 'call SetNormalTextColor("Yellow")'],
		\ [53, 'call SetNormalTextColor("Black")'],
		\ [54, 'call SetNormalTextColor("Cyan")'],
		\ [55, 'call SetNormalTextColor("Green")'],
		\ [56, 'call SetNormalTextColor("DarkGreen")']
		\ ])
		redraw
	endwhile
endfunction

function SetColorSchemeCommands()
	let l:retv = 1
	while 1 == l:retv
		if exists("g:colors_name")
			let l:colors_name = g:colors_name
		else
			let l:colors_name = '?'
		endif
		let l:retv = CommandMenu("-- ColorScheme! -- [ ".l:colors_name." ] /d:dark /l:light /0:default /1:delek /2:desert /3:elflord /4:murphy /5:pablo /6:ron /7:slate /8:torte",
		\ [
		\ [100, 'set background=dark | call s:HlModification()'],
		\ [108, 'set background=light | call s:HlModification()'],
		\ [48, 'call SetColorScheme("default")'],
		\ [49, 'call SetColorScheme("delek")'],
		\ [50, 'call SetColorScheme("desert")'],
		\ [51, 'call SetColorScheme("elflord")'],
		\ [52, 'call SetColorScheme("murphy")'],
		\ [53, 'call SetColorScheme("pablo")'],
		\ [54, 'call SetColorScheme("ron")'],
		\ [55, 'call SetColorScheme("slate")'],
		\ [56, 'call SetColorScheme("torte")']
		\ ])
		redraw
	endwhile
endfunction

function DisplayCommands()
	call CommandMenu("-- Display! -- Show /1:number(on,off) /2:cursorline(on,off) /3:cursorcolumn(on,off) | Color /t:normal_text /c:colorscheme",
		\ [
		\ [49, 'setlocal number!'],
		\ [50, 'setlocal cursorline!'],
		\ [51, 'setlocal cursorcolumn!'],
		\ [116, 'call NormalTextColorMenu()'],
		\ [99, 'call SetColorSchemeCommands()']
		\ ])
endfunction

function Commands()
	call CommandMenu("-- Commands! -- /f:file /e:edit /w:windows /t:tabs /d:display /c:clipboard | /s:save /q:quit /a:quit_all",
		\ [
		\ [102, 'call FileCommands()'],
		\ [101, 'call EditCommands()'],
		\ [119, 'call WindowCommands()'],
		\ [116, 'call TabCommands()'],
		\ [100, 'call DisplayCommands()'],
		\ [99, 'call ClipboardCommands()'],
		\ [115, 'update'],
		\ [113, 'confirm q'],
		\ [97, 'confirm qall']
		\ ])
endfunction

function ResizeWindow()
	let l:retv = 1
	while 1 == l:retv
		let l:retv = CommandMenu("-- ResizeWindow! -- /+:horizontal+5 /-:horizontal-5 /<:vertical+5 />:vertical-5",
			\ [
			\ [43, 'resize +5 | redraw'],
			\ [45, 'resize -5 | redraw'],
			\ [60, 'vertical resize +5 | redraw'],
			\ [62, 'vertical resize -5 | redraw']
			\ ])
	endwhile
endfunction

function BufferListCommands()
	let l:retv = 1
	if !exists("g:bufcmd_showlist")
		let g:bufcmd_showlist = 0
	endif
	while 1 == l:retv
		if(g:bufcmd_showlist)
			execute "buffers"
			" let g:bufcmd_showlist = 0
		endif
		let l:retv = CommandMenu("-- BufferList! -- /f:first /y:prev /x:next /l:last | :/s:show(:hide) | /c:close /a:close_all",
			\ [
			\ [102, 'confirm bfirst'],
			\ [121, 'confirm bnext'],
			\ [120, 'confirm bprevious'],
			\ [108, 'confirm blast'],
			\ [115, 'let g:bufcmd_showlist = (exists("g:bufcmd_showlist"))? !g:bufcmd_showlist : 1'],
			\ [99, 'confirm bwipeout'],
			\ [97, 'confirm %bwipeout']
			\ ])
		redraw
	endwhile
endfunction

function Control()
	call CommandMenu("-- Control! -- Tab /y:prev /x:next | Win /w:top /a:left /s:down /d:right | Resize /+/-:horiz /</>:vert | View /t:top /c:enter | /b:buffers",
		\ [
		\ [120, 'tabnext'],
		\ [121, 'tabprevious'],
		\ [119, 'wincmd k'],
		\ [97, 'wincmd h'],
		\ [115, 'wincmd j'],
		\ [100, 'wincmd l'],
		\ [43, 'resize +5 | redraw | call ResizeWindow()'],
		\ [45, 'resize -5 | redraw | call ResizeWindow()'],
		\ [60, 'vertical resize +5 | redraw | call ResizeWindow()'],
		\ [62, 'vertical resize -5 | redraw | call ResizeWindow()'],
		\ [116, 'normal! zt'],
		\ [99, 'normal! zz'],
		\ [98, 'call BufferListCommands()']
		\ ])
endfunction

autocmd BufNewFile,BufRead *.help set filetype=help
autocmd BufNewFile,BufRead *.note set filetype=help

noremap . :call Commands()<cr>
nnoremap <Enter> A
nnoremap <Del> i
nnoremap <Backspace> i
nnoremap <Space> i

nnoremap , :call Control()<cr>
nnoremap <C-c> :call Control()<cr>
inoremap <C-c> <ESC>:call Control()<cr>
vnoremap <C-c> <ESC>:call Control()<cr>

nnoremap öö "cyy
nnoremap ö "cyiw
vnoremap ö "cy
nnoremap Ö "syiw
vnoremap Ö "sy
nnoremap ß "cyydd
vnoremap ß "cygvd
nnoremap ä "cp
nnoremap ü "cP
vnoremap ä "cp
vnoremap ü "cP

call RunCustomScript()

